FROM olbat/cupsd

RUN apt-get update \
&& apt-get install -y \
  vim \
  printer-driver-cups-pdf \
  cmake \
  gcc \
  g++ \
  libcupsimage2-dev

RUN apt-get clean \
&& rm -rf /var/lib/apt/lists/*

COPY barcodedriver-1.2.06 /tscdriver

RUN cd /tscdriver && ./install-driver

RUN rm -rf /tscdriver

COPY cupsd.conf /etc/cups/

RUN sed -i 's/#ConfigFilePerm 0640/ConfigFilePerm 0666/' /etc/cups/cups-files.conf

COPY tmtr-3.0.0.0 /tmtr

RUN cd /tmtr && ./build.sh && ./install.sh

RUN rm -rf /tmtr

RUN rmdir /etc/cups/ppd \
&& cp -r /etc/cups /cups-config

COPY start.sh /

RUN chmod +x /start.sh

CMD ["/start.sh"]